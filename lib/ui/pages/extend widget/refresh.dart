part of '../pages.dart';

class Refresh extends StatefulWidget {
  @override
  _RefreshState createState() => _RefreshState();
}

class _RefreshState extends State<Refresh> {
  final listData = <User>[];
  List<Map> check = [];


  // @override
  // void initState() {
  //   listData..add(User('User 1'))..add(User('User 2'))..add(User('User 3'));
  //   super.initState();
  // }

  @override
  initState(){
    super.initState();
    Global.getData().then((value) {
      setState(() {
        check = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return  _buildWidgetListDataAndroid();
  }

  Widget _buildWidgetListDataAndroid() {
    return
      Column(children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () async{
                  if(check.where((e) => e['check'] == true).length == 0){
                    return;
                  }

                  LoadingBar.dialogLoading(context);

                  var data = await FmsDatabase.instance.readId(check);
                  await ApiService.synchBaps(data['baps']!)
                      .then((value) {
                    setState(() {
                      FmsDatabase.instance.dropBaps(data['baps']!.map((e) => e['baps_id'] as int).toList());
                      Global.bapsSynchStatus =  'Berhasil';
                    });
                  } ).onError((error, stackTrace) {
                    setState(() {
                      Global.bapsSynchStatus =  'Gagal';
                      print(error.toString());
                    });
                  });

                  await ApiService.synchSounding(data['sounding']!).then((value) {
                    setState(() {
                      FmsDatabase.instance.dropSounding(data['sounding']!.map((e) => e['sounding_id'] as int).toList());
                      // Global.getData().then((value) {
                      //   check = value;
                      // });
                      Global.soundingSynchStatus =  'Berhasil';
                    });
                  }).onError((error, stackTrace) {
                    setState(() {
                      Global.soundingSynchStatus =  'Gagal';
                    });
                  });

                  await ApiService.synchTransfer(data['transfer']!).then((value) {
                    setState(() {
                      FmsDatabase.instance.dropTransfer(data['transfer']!.map((e) => e['transfer_id'] as int).toList());
                      // Global.getData().then((value) {
                      //   check = value;
                      // });
                      Global.transferSynchStatus =  'Berhasil';
                    });
                  } ).onError((error, stackTrace) {
                    setState(() {
                      Global.transferSynchStatus =  'Gagal';
                    });
                  });

                  await ApiService.synchRefueling(data['refueling']!)
                      .then((value) {
                    setState(() {
                      FmsDatabase.instance.dropRefueling(data['refueling']!.map((e) => e['refueling_id'] as int).toList());
                      // Global.getData().then((value) {
                      //   check = value;
                      // });
                      Global.refuelingSynchStatus =  'Berhasil';
                    });
                  } ).onError((error, stackTrace) {
                    setState(() {
                      Global.refuelingSynchStatus =  'Gagal';
                    });
                  });

                  await ApiService.synchAttendance(data['attendance']!).then((value) {
                    setState(() {
                      FmsDatabase.instance.dropHistoryAttendance(data['attendance']!.map((e) => e['attendance_id'] as int).toList());
                      // Global.getData().then((value) {
                      //   check = value;
                      // });
                      Global.attendanceSynchStatus =  'Berhasil';
                    });
                  } ).onError((error, stackTrace) {
                    setState(() {
                      Global.attendanceSynchStatus =  'Gagal';
                    });
                  });

                  setState(() {
                    Global.getData().then((value) {
                      check = value;
                    });
                  });

                  if( Global.transferSynchStatus == 'Berhasil' && Global.attendanceSynchStatus == 'Berhasil'
                      && Global.soundingSynchStatus == 'Berhasil' && Global.refuelingSynchStatus == 'Berhasil'
                      && Global.bapsSynchStatus == 'Berhasil')
                  {
                    LoadingBar.hideLoadingDialog(context);
                    _dialogUploadAlert();
                  }else if( Global.transferSynchStatus == 'Gagal' || Global.attendanceSynchStatus == 'Gagal'
                      || Global.soundingSynchStatus == 'Gagal' || Global.refuelingSynchStatus == 'Gagal'
                      || Global.bapsSynchStatus == 'Gagal'){
                    LoadingBar.hideLoadingDialog(context);
                    _dialogFailAlert();
                  }

                },
                child: Text('Upload All',
                    style: TextStyle(
                        color: Color(0xffF0C419),
                        fontFamily: Fonts.REGULAR,
                        fontSize: 18)),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: RefreshIndicator(
          onRefresh: refreshData,
            child:  ListView.builder(
                    shrinkWrap: false,
                    padding: const EdgeInsets.all(8),
                    itemCount: check.length  ,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        color: Colors.white,
                        elevation: 0.8,
                        shadowColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: InkWell(
                            onTap: () {
                              print('Card tapped.');
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 20),
                                  child: SizedBox(
                                    height: 70,
                                    width: 30,
                                    child: CheckboxListTile(
                                      title: Text(""),
                                      value: check[index]['check'],
                                      onChanged: (bool? value) {
                                        if(value != null) {
                                          setState(() {
                                            check[index]['check'] = value;
                                          });
                                        }
                                      },
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        check[index]['text'],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(color: Colors.black,
                                            fontFamily: Fonts.REGULAR,fontSize: 18)
                                    ),
                                    Text(
                                        check[index]['created_at'],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: Colors.grey,
                                            fontFamily: Fonts.REGULAR,fontSize: 12)
                                    ),
                                  ],
                                ),
                              ],
                            )
                        ),
                      );
                    }) ),
        )
      ],
      );


  }

  ///pop up status
  Future<void> _dialogUploadAlert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Berhasil!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Berhasil upload data"),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Oke'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  ///pop up status
  Future<void> _dialogFailAlert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Oops!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Gagal upload data. Periksa data yang diinputkan atau jaringan Anda!"),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Oke'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildWidgetItemListData(User user, BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(user.nama),
          ],
        ),
      ),
    );
  }

  Future refreshData() async {
    listData.clear();
    await Future.delayed(Duration(seconds: 2));
    setState(() {});
  }
}

class User {
  final String nama;

  User(this.nama);
}
